# Déroulement de l'examen

## Manipulations Git / Gitlab (7 points)

Vous devez tout d'abord créer un fork **privé** de ce dépôt sur votre compte gitlab.

Vous devez ensuite m'ajouter (l'utilisateur à ajouter est celui qui est propriétaire de ce dépôt) en tant que **mainteneur** du dépôt afin que j'ai accès à votre travail.

- Vous devez répondre aux questions de la première partie dans une branche "exam-part-1".
- Vous devez répondre aux questions de la seconde partie dans une branche "exam-part-2".
- Vous devez répondre aux questions de la troisième partie dans une branche "exam-part-3".

Une fois que c'est fait, vous devez merger les trois branches dans une branche "answers" **en utilisant les Merge Request** et **sans supprimer les branches d'origine**.

Enfin, **pour conclure l'examen** vous devez ajouter votre nom dans le README sur la branche "main", tout en haut du fichier.

Si vous ne réussissez pas à gérer les branches comme il le faut, **pas de panique**. Vous pouvez copier le texte et m'envoyer les réponses dans Teams.

Cette première partie (toutes les manipulations Git / Gitlab décrites au dessus) est sur **7 points**.

## Questions (10 points - Détails de la répartition des 10 points en dessous)

Les réponses aux questions doivent se faire directement dans le fichier `README.md`, dans la branche qui correspond.

Dans les deux premières parties, répondez aux questions **en ne laissant que la ou les bonnes réponses**.

⚠️ Si ce format n'est pas respecté, je ne compte pas la réponse comme étant bonne. ⚠️

⚠️ S'il n'y a aucune bonne réponse, écrivez à la place des réponses "Pas de bonne réponse". ⚠️

Dans la troisième partie, insérez la réponse sous la questions. Vous devez utiliser **vos propres mots / phrases** pour répondre, pas de copier coller.

### Première partie (il y a **au plus** une seule bonne réponse, les mauvaise réponses coûtent des points) (4 points)

1. Parmi ces commandes, laquelle peut être utilisée pour obtenir les informations d'un dépôt, mais ne fusionne pas automatiquement les changements ?

- A. git pull
- B. git download
- C. git clone
- D. git fetch

2. Quelle commande est couramment employée pour propager des modifications vers une branche distante, mais ne fonctionne que si le dépôt local est à jour?

- A. git propagate
- B. git clone
- C. git fetch
- D. git send

3. Quelle commande permet de vérifier dans quel état est notre dépôt local ?

- A. git status
- B. git state
- C. git check
- D. git verify

4. Dans quelle zone doit-on placer les éléments pour les sauvegarder avant de les partager ?

- A. Dépôt local
- B. Dépôt distant
- C. Stage
- D. Zone de travail

5. Comment démarrer un nouveau projet avec git ?

- A. git initialize
- B. git begin
- C. git init
- D. git clone

6. Comment récupérer uniquement les dernières mise à jour d’un dépôt ?

- A. git pull
- B. git remote get
- C. git patch
- D. git get

7. Avec quelle commande ajoute-t-on ses modifications avant de pouvoir faire un commit ?

- A. git modify
- B. git amend
- C. git push
- D. git commit

8. Quelle commande vous aiderait à identifier l'auteur d'une modification sur une ligne spécifique d'un fichier ?

- A. git diff
- B. git log
- C. git find
- D. git blame

9. Quelle commande permet d’ajouter un message à un commit ?

- A. git commit "mon message"
- B. git commit -m "mon message"
- C. git add "mon message"
- D. git commit message "mon message"

10. Nous venons de créer un nouveau fichier appelé «fichier.html». Lequel des éléments suivants ajoutera ce fichier afin que nous puissions le commiter dans git ?

- A. git add -a fichier.html 
- B. git new fichier.html
- C. git new -a fichier.html
- D. git add fichier.html 

11. Pour basculer vers une branche principale nommée "main" en évitant la création d'une nouvelle branche, quelle commande utiliseriez-vous ?

- A. git checkout main 
- B. git checkout main --no-branch
- C. git checkout origin 
- D. git checkout -b main 

12. Si un commit a été envoyé (push) et que vous souhaitez revenir en arrière tout en préservant l'historique, quelle commande utiliseriez-vous ?

- A. git reset --hard origin/master 
- B. git revert <commit> 
- C. git reset <commit> 
- D. git uncommit 

13. Quelle est la commande pour annuler un commit s’il n’est pas encore dans le dépôt distant ?

- A. git reset --hard origin/master 
- B. git revert <commit> 
- C. git reset <commit> 
- D. git init 

14. Comment créer une nouvelle branche et y basculer directement ?

- A. git checkout <branch-name> 
- B. git branch <branch-name> 
- C. git checkout -c <branch-name> 
- D. git branch -c <branch-name> 

15. Comment ajouter un nouveau dépôt distant ?

- A. git init -a <name> <url> 
- B. git repository add <name> <url> 
- C. git remote add <name> <url> 
- D. git remote init <name> <url> 

### Deuxième partie : A partir de là plusieurs choix sont parfois possibles (et les mauvaises réponses coûtent toujours des points !) (4 points)

16. Qu’est-ce qu’une branche dans git ?

- A. Un index
- B. Un espace de travail
- C. C’est un pointeur vers un commit
- D. Une partie d’un arbre

17. Comment faire pour rassembler (squasher) plusieurs commits en un seul ?

- A. git merge commit1 commit2 … commitN
- B. git replace commit1
- C. git rebase -i
- D. git rebase merge

18. A quoi sert un tag dans git ?

- A. Marquer un point spécifique du développement pour pouvoir y revenir facilement
- B. Ajouter un fichier PNG d’illustration
- C. Marquer une version de livraison du produit
- D. Garder la trace d’une branche que l’on souhaite supprimer

19. A quoi sert un git rebase ?

- A. Réinitialiser une branche
- B. Recréer les commits d’une branche sur une autre
- C. Déplacer des commits d’une branche sur une autre
- D. Regrouper des commits

20. Si vous avez accidentellement commis des modifications et que vous souhaitez modifier ce commit sans en créer un nouveau, quelle commande devriez-vous utiliser ?

- A. git commit --amend
- B. git reset HEAD~1
- C. git revert
- D. git reflog

21. Le développement du projet est réalisé dans une branche dev. La version distribuée est celle de la branche master. On souhaite intégrer les nouveaux développements dans la version distribuée. Que doit on faire ?

- A. git branch dev; git merge master
- B. git branch master; git merge dev
- C. git checkout master;   git merge dev
- D. git merge master

22. Comment enlever un fichier d'un commit que l'on vient d'effectuer, sans supprimer le fichier du répertoire de travail ?

- A. git rm fichier; git commit --amend
- B. git reset --soft HEAD^; git reset HEAD fichier; git commit
- C. git checkout HEAD^; git reset HEAD fichier; git commit
- D. git revert fichier; git commit

23. Dans le modèle standard git, dans quelle(s) branche(s) doit-on merger une branche « hotfix » ?

- A. master
- B. develop
- C. feature
- D. release

24. git est le meilleur outil de gestion de version !

- A. VRAI
- B. FAUX
- C. C’est plus compliqué que ça
- D. Réponse D


### Troisième partie : Questions à réponse libre (à vous d'estimer la taille des réponses à fournir) (2 points)

25. Comment un conflit dans git peut-il être résolu ? (bonnes pratiques, commandes, méthodes, etc.)
  
  
  

26. Pour supprimer une branche, quelle commande est utilisée ?
  
  
  

27. Quelles sont les étapes pour effectuer des changements dans un dépôt Github qui ne vous appartient pas ?
  
  
  

28. A quoi sert « .gitignore » ?
  
  
  

29. Quels sont les inconvénients à utiliser git ?
  
  
  

30. Qu’est-ce qu’un « fork » ?
  
  


### Quatrième partie : REVERT (3 points)

Faites un revert du commit de ce dépôt qui a eu lieu sur la branche main dont le message de commit est **"revert me"**.
Une nouvelle section devrait apparaître au dessus de ce texte, répondez à la question et l'examen sera quasiment terminé !
